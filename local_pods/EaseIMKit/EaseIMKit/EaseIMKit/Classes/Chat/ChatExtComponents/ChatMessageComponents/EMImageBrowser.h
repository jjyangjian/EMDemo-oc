//
//  EMImageBrowser.h
//  ChatDemo-UI3.0
//
//  Created by XieYajie on 2019/1/29.
//  Copyright © 2019 XieYajie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EasePhotoBrowser.h"

NS_ASSUME_NONNULL_BEGIN


typedef enum {
    EMImageBrowserModelSourceTypeImage = 0,
    EMImageBrowserModelSourceTypeURLString,
} EMImageBrowserModelSourceType;
@interface EMImageBrowserModel : NSObject
@property (nonatomic)EMImageBrowserModelSourceType sourceType;
@property (nonatomic)CGSize size;
@property (nonatomic,copy)NSString *urlString;
@property (nonatomic,strong)UIImage *image;
@end



@interface EMImageBrowser : NSObject

+ (instancetype)sharedBrowser;

- (void)showImages:(NSArray<UIImage *> *)aImageArray
    fromController:(UIViewController *)aController;


- (void)showImageModles:(NSArray<EMImageBrowserModel *> *)models
    fromController:(UIViewController *)aController;





- (void)dismissViewController;

@end

NS_ASSUME_NONNULL_END
