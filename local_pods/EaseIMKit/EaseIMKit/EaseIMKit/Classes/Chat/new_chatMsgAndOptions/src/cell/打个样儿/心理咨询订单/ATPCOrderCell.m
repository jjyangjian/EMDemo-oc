//
//  ATPCOrderCell.m
//  EaseIMKit
//
//  Created by 杨剑 on 2024/1/15.
//Psychological counseling
//Order for goods
//
#import "ATPCOrderCell.h"

@interface ATPCOrderCell ()

@property (nonatomic,strong)UIView *msgContentView;
@property (nonatomic,strong)UIImageView *bubbleView;


@end

@implementation ATPCOrderCell


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self configMsgSubViews];
    }
    return self;
}

- (void)configMsgSubViews{
    self.msgContentView = UIView.new;
    [self.msgBackgroundView addSubview:self.msgContentView];

//    self.textView = UITextView.new;
//    self.textView.backgroundColor = UIColor.clearColor;
//    self.textView.textContainer.lineFragmentPadding = 0;
//    self.textView.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0);
//    self.textView.editable = false;
//    self.textView.scrollEnabled = false;
//    self.textView.userInteractionEnabled = false;
//    [self.textView setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
//    [self.textView setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
//    [self.textView setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
//    [self.textView setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];

    
    
    UIView *topBgView = UIView.new;
    [self.msgContentView addSubview:topBgView];
    topBgView.backgroundColor = UIColor.cyanColor;
    [topBgView Ease_makeConstraints:^(EaseConstraintMaker *make) {
        make.top.Ease_equalTo(0);
        make.left.Ease_equalTo(0);
        make.right.Ease_equalTo(0);
        make.height.Ease_equalTo(60);
    }];
    
    //计算高度:
    //自上而下 content内的高度 60 + ?
    
    UILabel *dingdanshijianTitleLabel = UILabel.new;
    [topBgView addSubview:dingdanshijianTitleLabel];
    [dingdanshijianTitleLabel Ease_makeConstraints:^(EaseConstraintMaker *make) {
        make.centerY.Ease_equalTo(0);
        make.left.Ease_equalTo(12);
    }];
    
    UILabel *timeLabel = UILabel.new;
    [topBgView addSubview:timeLabel];
    [timeLabel Ease_makeConstraints:^(EaseConstraintMaker *make) {
        make.centerY.Ease_equalTo(0);
        make.right.Ease_equalTo(-12);
    }];
    
    UILabel *fenxiLabel = UILabel.new;
    [self.msgContentView addSubview:fenxiLabel];
    [fenxiLabel Ease_makeConstraints:^(EaseConstraintMaker *make) {
        make.top.Ease_equalTo(topBgView.ease_bottom).offset(10);
        make.left.Ease_equalTo(12);
        make.right.Ease_equalTo(12);
        make.height.Ease_equalTo(60);
    }];
    //计算高度:
    //自上而下 content内的高度 60 + 10 + 60

    
    
    
    UIView *tagBgView = UIView.new;
    [self.msgContentView addSubview:tagBgView];
    tagBgView.backgroundColor = UIColor.lightGrayColor;
    [tagBgView Ease_makeConstraints:^(EaseConstraintMaker *make) {
        make.top.Ease_equalTo(fenxiLabel.ease_bottom).offset(10);
        make.left.Ease_equalTo(12);
        make.right.Ease_equalTo(-12);
        make.height.Ease_equalTo(60);
    }];

    //计算高度:
    //自上而下 content内的高度 60 + 10 + 60 + 10 + 60

    
    UILabel *tagLabel = UILabel.new;
    [tagBgView addSubview:tagLabel];
    [tagLabel Ease_makeConstraints:^(EaseConstraintMaker *make) {
        make.centerY.Ease_equalTo(0);
        make.right.Ease_equalTo(-12);
    }];

    UILabel *moneyTitleLabel = UILabel.new;
    [self.msgContentView addSubview:moneyTitleLabel];
    [moneyTitleLabel Ease_makeConstraints:^(EaseConstraintMaker *make) {
        make.top.Ease_equalTo(tagBgView.ease_bottom).offset(10);
        make.left.Ease_equalTo(24);
        make.height.Ease_equalTo(30);
        make.bottom.Ease_equalTo(0);
    }];
    //计算高度:
    //自上而下 content内的高度 60 + 10 + 60 + 10 + 60 + 10 + 30 (此为最终值)

    
    UILabel *moneyContentLabel = UILabel.new;
    [self.msgContentView addSubview:moneyContentLabel];
    [moneyContentLabel Ease_makeConstraints:^(EaseConstraintMaker *make) {
        make.centerY.Ease_equalTo(moneyTitleLabel);
        make.left.Ease_equalTo(moneyTitleLabel.ease_right).offset(2);
        make.height.Ease_equalTo(30);
    }];

    UIButton *goButton = UIButton.new;
    [self.msgContentView addSubview:goButton];
    [goButton Ease_makeConstraints:^(EaseConstraintMaker *make) {
        make.centerY.Ease_equalTo(moneyTitleLabel);
        make.right.Ease_equalTo(-12);
        make.height.Ease_equalTo(30);
    }];
    goButton.backgroundColor = UIColor.greenColor;
    [goButton setTitle:@"去支付" forState:UIControlStateNormal];
    
    
    dingdanshijianTitleLabel.text = @"订单时间";
    timeLabel.text = @"2020-10-10";
    fenxiLabel.text = @"分析详情";
    tagLabel.text = @"情感分析";
    moneyTitleLabel.text = @"合计:¥";
    moneyContentLabel.text = @"120.00";
//    [self.msgContentView addSubview:self.textView];
    
    [self configStateView];
    [self configBubble];
}

- (void)configStateView{
    [self.stateLabel Ease_remakeConstraints:^(EaseConstraintMaker *make) {
        make.bottom.Ease_equalTo(self.msgContentView.ease_bottom);
        make.right.Ease_equalTo(self.msgContentView.ease_left).offset(-20);
    }];
}

- (void)configBubble{
    self.bubbleView = UIImageView.new;
    [self.msgBackgroundView insertSubview:self.bubbleView belowSubview:self.msgContentView];
    {
        self.msgContentView.userInteractionEnabled = false;
        self.bubbleView.userInteractionEnabled = true;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(messageTapGestureClick:)];
        UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(messagePressGestureClick:)];
        [self.bubbleView addGestureRecognizer:tapGesture];
        [self.bubbleView addGestureRecognizer:longPressGesture];
    }
}

- (void)messageTapGestureClick:(UITapGestureRecognizer *)tapGesture{
    [super messageTapGestureClick:tapGesture];
}
- (void)messagePressGestureClick:(UILongPressGestureRecognizer *)longPressGesture{
    [super messagePressGestureClick:longPressGesture];
}


- (void)resetSubViewsLayout:(EMMessageDirection)direction showHead:(BOOL)showHead showName:(BOOL)showName{
    [super resetSubViewsLayout:direction showHead:showHead showName:showName];
    UIEdgeInsets msgContentEdgeInsets = [EMsgTableViewFunctions
                                         convertToEdgeInsets_direction:direction top:EMsgCellLayoutAdapterConfigs.shared.contentLayoutAdapter.top fromSide:EMsgCellLayoutAdapterConfigs.shared.contentLayoutAdapter.fromSide toSide:EMsgCellLayoutAdapterConfigs.shared.contentLayoutAdapter.toSide bottom:EMsgCellLayoutAdapterConfigs.shared.contentLayoutAdapter.bottom];
    [self.msgContentView Ease_remakeConstraints:^(EaseConstraintMaker *make) {
        make.top.Ease_equalTo(msgContentEdgeInsets.top);
        make.bottom.Ease_equalTo(-msgContentEdgeInsets.bottom);
        make.width.Ease_equalTo(200);
        switch (direction) {
            case EMMessageDirectionSend:{
                make.left.Ease_greaterThanOrEqualTo(msgContentEdgeInsets.left);
                make.right.Ease_equalTo(-msgContentEdgeInsets.right);
                break;
            }
            case EMMessageDirectionReceive:{
                make.left.Ease_equalTo(msgContentEdgeInsets.left);
                make.right.Ease_lessThanOrEqualTo(-msgContentEdgeInsets.right);
                break;
            }
            default:
                break;
        }
    }];

//    [self.textView Ease_remakeConstraints:^(EaseConstraintMaker *make) {
//        make.top.Ease_equalTo(0);
//        switch (direction) {
//            case EMMessageDirectionSend:
//                make.left.Ease_greaterThanOrEqualTo(0);
//                make.right.Ease_equalTo(0);
//                break;
//            case EMMessageDirectionReceive:
//                make.left.Ease_equalTo(0);
//                make.right.Ease_lessThanOrEqualTo(0);
//                break;
//            default:
//                break;
//        }
//        make.bottom.Ease_equalTo(0);
//    }];

    UIEdgeInsets bubbleEdgeInsets =
    [EMsgTableViewFunctions
                                         convertToEdgeInsets_direction:direction
     top:EMsgCellBubbleLayoutAdapterConfigs.shared.currentAdapter.top
     fromSide:EMsgCellBubbleLayoutAdapterConfigs.shared.currentAdapter.fromSide
     toSide:EMsgCellBubbleLayoutAdapterConfigs.shared.currentAdapter.toSide
     bottom:EMsgCellBubbleLayoutAdapterConfigs.shared.currentAdapter.bottom];
    
    [self.bubbleView Ease_remakeConstraints:^(EaseConstraintMaker *make) {
        make.top.Ease_equalTo(self.msgContentView).offset(-bubbleEdgeInsets.top);
        make.left.Ease_equalTo(self.msgContentView).offset(-bubbleEdgeInsets.left);
        make.bottom.Ease_equalTo(self.msgContentView).offset(bubbleEdgeInsets.bottom);
        make.right.Ease_equalTo(self.msgContentView).offset(bubbleEdgeInsets.right);
    }];
    self.bubbleView.image = [EMsgCellBubbleLayoutAdapterConfigs.shared.currentAdapter bubbleImage:direction];
}


- (void)bindDataFromViewModel:(EMsgBaseCellModel *)model{
    [self resetSubViewsLayout:model.direction
                     showHead:[EMsgTableViewConfig.shared showHead_chatType:model.message.chatType direction:model.direction]
                     showName:[EMsgTableViewConfig.shared showName_chatType:model.message.chatType direction:model.direction]];
    [super bindDataFromViewModel:model];

    EMCustomMessageBody *body = (EMCustomMessageBody *)model.message.body;

    
    
    
//    EMTextMessageBody *body = (EMTextMessageBody *)model.message.body;
//    self.textView.attributedText =
//    [EMsgTableViewFunctions attributedString:body.text font:EMsgTableViewConfig.shared.textFont color:UIColor.blackColor];
    
//    attributedString(body.text, EMsgTableViewConfig.shared.textFont, UIColor.blackColor);
}


@end
