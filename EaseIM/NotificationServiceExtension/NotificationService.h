//
//  NotificationService.h
//  NotificationServiceExtension
//
//  Created by yangjian on 2023/7/13.
//  Copyright © 2023 yangjian. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
