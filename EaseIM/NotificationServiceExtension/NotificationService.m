//
//  NotificationService.m
//  NotificationServiceExtension
//
//  Created by yangjian on 2023/7/13.
//  Copyright © 2023 yangjian. All rights reserved.
//

#import "NotificationService.h"
#import <UIKit/UIKit.h>
@interface NotificationService ()

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;

@end

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];
    
    // Modify the notification content here...
    self.bestAttemptContent.title = [NSString stringWithFormat:@"%@ [modified]", self.bestAttemptContent.title];
    self.bestAttemptContent.body = [[NSString stringWithFormat:@"<f[%@]-t[%@]>",request.content.userInfo[@"f"],request.content.userInfo[@"t"]] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
//    if ([request.content.userInfo[@"f"] isEqualToString:@"em"]) {
//        self.bestAttemptContent.title = @"";
//        self.bestAttemptContent.body = @"";
//        self.bestAttemptContent.sound = nil;
//    }
    
    
    self.bestAttemptContent.subtitle = [NSString stringWithFormat:@"子标题:%@",request.content.userInfo[@"e"]];
    
    
    //...这里添加图片
    self.bestAttemptContent.attachments = @[];
    
    NSUserDefaults *userDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.yyytp.dabaojian"];
    long push_badge = [userDefaults integerForKey:@"push_badge"];
    push_badge += 2;
    [userDefaults setInteger:push_badge forKey:@"push_badge"];
    self.bestAttemptContent.badge = [NSNumber numberWithLong:push_badge];
    self.contentHandler(self.bestAttemptContent);
}

- (void)serviceExtensionTimeWillExpire {
    // Called just before the extension will be terminated by the system.
    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
    self.contentHandler(self.bestAttemptContent);
}

@end
