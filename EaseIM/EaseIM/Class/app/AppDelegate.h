/************************************************************
  *  * Hyphenate CONFIDENTIAL 
  * __________________ 
  * Copyright (C) 2016 Hyphenate Inc. All rights reserved. 
  *  
  * NOTICE: All information contained herein is, and remains 
  * the property of Hyphenate Inc.
  * Dissemination of this information or reproduction of this material 
  * is strictly forbidden unless prior written permission is obtained
  * from Hyphenate Inc.
 
 iOS聊天界面,显示消息列表,重构消息cell单元格,看懂这块儿妥妥能花式实现自定义消息cell,从此自定义消息简单到姥姥家<<基于环信IM iOS Demo 重构messageCell方案>>
 https://www.imgeek.org/article/825360517
  */

#import <UIKit/UIKit.h>


#import "EMAppConfig.h"
#import "EaseIMHelper.h"
#import "EMAppCallHelper.h"
#import "EMUserNotificationHelper.h"



@interface AppDelegate : UIResponder <UIApplicationDelegate, EMChatManagerDelegate>
{
    EMConnectionState _connectionState;
}

@property (strong, nonatomic) UIWindow *window;

@end
