//
//  YAxis.swift
//  Charts
//
//  Copyright 2015 Daniel Cohen Gindi & Philipp Jahoda
//  A port of MPAndroidChart for iOS
//  Licensed under Apache License 2.0
//
//  https://github.com/danielgindi/Charts
//

import Foundation


/*
 
 {
     'channel_channel': '1118221212161653#mars_30adca12dfa742d1b2d87c404a658e06@easemob.com',
     'os': 'ios',
     'ip': '60.213.94.54',
     'sdk_service_id':
     'f05e4036-b701-4649-b7b0-8ef7280c7c7e',
     'meta_ext': {
         'chat_type': 'chat:user',
         'write_channel': True,
         'route_type': 'route_normal',
         'msg_type': 'custom',
         'chat_route':
         'route_normal:chat:user:custom',
         'client_id': 16946135997900221
     },
     'version': '4.0.0',
     'channel_user': '1118221212161653#mars_96e73490926943c89eca5d977d82eabd@easemob.com',
     'chat_type': 'chat',
     'is_downgrade': False,
     'content_type': 'chat:user:custom',
     'payload': {
          'ext': {
              'eventJson': '{"content":"那是穿的白天穿的\\n那个讲究劲的，不得换上睡衣吗？"}',
              'event': 'RC:TxtMsg_em'},
         'bodies': [{
             'customEvent': 'RC:TxtMsg_em',
             'type': 'custom'
         }],
         'meta': {},
         'from': '96e73490926943c89eca5d977d82eabd',
         'to': '30adca12dfa742d1b2d87c404a658e06',
         'type': 'chat'
     },
     'writed_channel': False,
     'from': '96e73490926943c89eca5d977d82eabd',
     'to': '30adca12dfa742d1b2d87c404a658e06',
     'msg_id': '1189915534733346240',
     'direction': 'outgoing',
     'timestamp': 1694613599866
 }
 */
